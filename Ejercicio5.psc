Algoritmo Ejercicio5
	Definir km_recorridos Como Entero;
	Definir precio_g Como Entero;
	Definir tiempo_h Como Entero;
	Definir tiempo_m Como Entero;
	
	Escribir "Inserte los km recorridos: ";
	leer km_recorridos;
	Escribir "Ingrese el precio del litro de gasolina: ";
	leer precio_g;
	Escribir "Indique el tiempo en horas: ";
	leer tiempo_h;
	Escribir "Indique el tiempo en minutos: ";
	leer tiempo_m;
	consumo_de_gasolina_por_km_en_bs <- precio_g/km_recorridos;
    consumo_de_gasolina_por_km_en_litros <- consumo_de_gasolina_por_km_en_bs*precio_g;
	velocidad_media_en_km_h <- km_recorridos/(tiempo_h+tiempo_m/60);
    velocidad_media_en_m_s <- km_recorridos*1000/(tiempo_h*60*60+tiempo_m*60);
    
    Escribir "Valor de consumo de gasolina por km en litros: ", consumo_de_gasolina_por_km_en_litros;
    Escribir "Valor de consumo de gasolina por km en bs: ", consumo_de_gasolina_por_km_en_bs;
    Escribir "Valor de velocidad media en km h: ", velocidad_media_en_km_h;
    Escribir "Valor de velocidad media en m s: ", velocidad_media_en_m_s;
	

FinAlgoritmo
